local wInt, wStr = gkini.WriteInt, gkini.WriteString
local tc, ti, tr, ts = table.concat, table.insert, table.remove, table.sort

local ini_branch = irc.iniBranch

local function command_list(cmd_branch)
	local list = {}
	for k in pairs(cmd_branch) do
		if (k ~= '_default') then ti(list, k) end
	end
	ts(list)
	return tc(list, ', ')
end

local function tostring2(val)
	local t, r = type(val)
	if (t == 'boolean') then 
		r = (val and 1) or 0
	elseif (t == 'string') then
		r = '"'..tostring(val)..'"'
	else
		r = tostring(val)
	end
	return r
end

local function encode_string(str)
	if (str == '') then
		return '%e'
	else
		return str:gsub('^(%s*)(.-)(%s*)$', function(s1, w, s2) return string.rep('%s', s1:len())..w..string.rep('%s', s2:len()) end)
	end
end

local function help(cmd_branch, invalid)
	if (invalid) then print('\127ffffffInvalid command!') end
	print('\127ffffffCommands: '..command_list(cmd_branch))
end


local function update_value(newvalue, table, key, inikey, syntax)
	local t = type(newvalue)
	if (newvalue or t == 'boolean') then
		local oldvalue = table[key]
		table[key] = newvalue
		if (t == 'number') then
			wInt(ini_branch, inikey, newvalue)
		elseif (t == 'boolean') then
			wInt(ini_branch, inikey, (newvalue and 1) or 0)
		else
			wStr(ini_branch, inikey, encode_string(newvalue))
		end
		print('\127ffffff'..key..' changed.  New value is '..tostring2(newvalue))
		return true, oldvalue
	else
		print('\127ffffffCurrent value is '..tostring2(table[key])..'\n/'..irc.CommandName..' '..syntax)
		return false
	end
end

local function print_reload()
	print('\127ffffffThis change requires a reload to take effect.')
end

local cli_commands = {
	['start'] = function(args)
		--test.print("cli.start("..tc(args, ',')..')')
		if (irc.VOConnected) then
			print('\127ffffffIRC relay starting')
			irc.connect()
		else
			console_print('You must be connected to the VO server before starting the relay.')
		end
	end,
			
	['stop'] = function(args)
		--test.print("cli.stop("..tc(args, ',')..')')
		print('\127ffffffIRC relay stopping')
		irc.disconnect()
	end,
			
-- 	['restart'] = function(args)
-- 		--test.print(tc{"cli.restart(",tc(args, ','),')'})
-- 		print('\127ffffffIRC relay restarting')
-- 		irc.disconnect()
-- 		irc.connect()
-- 	end,
			
-- 	['settings'] = function(args)
-- 		--test.print(tc{"cli.settings(",tc(args, ','),')'})
-- 	end,
			
	['set'] = {
		['username'] = function(args)
			--test.print(tc{"cli.set.username(",tc(args, ','),')'})
			local newvalue = args[1]
			update_value(newvalue, irc, 'Username', 'Username', 'set username <string>')
		end,
				
		['nickname'] = function(args)
			--test.print(tc{"cli.set.nickname(",tc(args, ','),')'})
			local newvalue = args[1]
			update_value(newvalue, irc, 'Nickname', 'Nickname', 'set nickname <string>')
		end,
				
		['password'] = function(args)
			--test.print(tc{"cli.set.password(",tc(args, ','),')'})
			local newvalue = args[1]
			update_value(newvalue, irc, 'Password', 'Password', 'set password <string>')
		end,

		['debug'] = function(args)
			--test.print(tc{"cli.set.debug(",tc(args, ','),')'})
			local newvalue = tonumber(args[1])
			update_value(newvalue, irc, 'Debug', 'Debug', 'set debug <integer>')
		end,
				
		['hidepms'] = function(args)
			--test.print(tc{"cli.set.debug(",tc(args, ','),')'})
			local newvalue = tonumber(args[1])
			if (newvalue) then newvalue = newvalue == 1 end
			update_value(newvalue, irc, 'HidePMs', 'HidePMs', 'set hidepms <0|1>')
		end,

		['autostart'] = function(args)
			--test.print(tc{"cli.set.autostart(",tc(args, ','),')'})
			local newvalue = tonumber(args[1])
			if (newvalue) then newvalue = newvalue == 1 end
			update_value(newvalue, irc, 'AutoStart', 'AutoStart', 'set autostart <0|1>')
		end,
				
		['vomsgprefix'] = function(args)
			--test.print(tc{"cli.set.vomsgprefix(",tc(args, ','),')'})
			local newvalue = #args > 0 and tc(args, ' ') or nil
			if (newvalue) then newvalue = newvalue:gsub('^([\'\"]).*%1$', '') end
			update_value(newvalue, irc, 'VOMsgPrefix', 'VOMsgPrefix', 'set vomsgprefix <string>')
		end,
				
		['plugindir'] = function(args)
			--test.print(tc{"cli.set.plugindir(",tc(args, ','),')'})
			local newvalue = #args > 0 and tc(args, ' ') or nil
			if (newvalue) then
				if (not newvalue:match('/$')) then newvalue = newvalue..'/' end
				if (not newvalue:match('^plugins/')) then newvalue = 'plugins/'..newvalue end
			end
			if (update_value(newvalue, irc, 'PluginDirectory', 'PluginDirectory', 'set plugindir <string>/')) then
				print_reload()
			end
		end,
			
				
		['_default'] = function(cmd_branch, args)
			--test.print(tc{"cli.set._default(",tc(args, ','),')'})
			return help(cmd_branch, args[1])
		end,
	},
	
	['auth'] = {
		['require'] = function(args)
			--test.print(tc{"cli.auth.require(",tc(args, ','),')'})
			local newvalue = tonumber(args[1])
			if (newvalue) then newvalue = newvalue == 1 end
			update_value(newvalue, irc, 'RequireAuth', 'RequireAuth', 'auth require <0|1>')
			for server, sock in pairs(irc.Sockets) do
				if (irc.RequireAuth) then
					sock:IRCSend('NAMES '..sock.Channel) -- TODO update to affect all joined channels when supported
				end
				sock:IRCSend('MODE '..irc.Nickname..(irc.RequireAuth and ' +' or ' -')..'R')
			end
			irc.SetCommandLevels()
			irc.SetCustomCommandLevels()
		end,
				
		['link'] = function(args)
			--test.print(tc{"cli.auth.link(",tc(args, ','),')'})
			local newvalue = tonumber(args[1])
			if (newvalue) then newvalue = newvalue == 1 end
			update_value(newvalue, irc, 'RequireLink', 'RequireLink', 'auth link <0|1>')
			irc.SetCommandLevels()
			irc.SetCustomCommandLevels()
		end,
		
		['authdelay'] = function(args)
			--test.print(tc{"cli.auth.authdelay(",tc(args, ','),')'})
			local newvalue = tonumber(args[1])
			update_value(newvalue, irc, 'AuthDelay', 'AuthDelay', 'auth authdelay <integer>')
		end,

		['reauthdelay'] = function(args)
			--test.print(tc{"cli.auth.reauthdelay(",tc(args, ','),')'})
			local newvalue = tonumber(args[1])
			update_value(newvalue, irc, 'ReauthDelay', 'ReauthDelay', 'auth reauthdelay <integer>')
		end,
				
		['expirelink'] = function(args)
			--test.print(tc{"cli.auth.expirelink(",tc(args, ','),')'})
			local newvalue = tonumber(args[1])
			update_value(newvalue, irc, 'ExpireLink', 'ExpireLink', 'auth expirelink <integer>')
		end,
				
		['multiplier'] = function(args)
			--test.print(tc{"cli.auth.multiplier(",tc(args, ','),')'})
			local newvalue = tonumber(args[1])
			update_value(newvalue, irc, 'ExpireLinkMultiplier', 'ExpireLinkMultiplier', 'auth multiplier <integer>')
		end,

		['_default'] = function(cmd_branch, args)
			--test.print(tc{"cli.set._default(",tc(args, ','),')'})
			return help(cmd_branch, args[1])
		end,
			
	},
	
	['server'] = {
		['defaultport'] = function(args)
			--test.print(tc{"cli.server.defaultport(",tc(args, ','),')'})
			local newvalue = tonumber(args[1])
			update_value(newvalue, irc.Servers, 'DefaultPort', 'Servers.DefaultPort', 'server defaultport <integer>')
		end,
				
		['defaultchannel'] = function(args)
			--test.print(tc{"cli.server.defaultchannel(",tc(args, ','),')'})
			local newvalue = args[1]
			local r, oldvalue = update_value(newvalue, irc.Servers, 'DefaultChannel', 'Servers.DefaultChannel', 'server defaultchannel <string>')
			if (r) then
				for server, sock in pairs(irc.Sockets) do
					if (sock.Channel == oldvalue) then
						sock:IRCSend('PART '..oldvalue)
						sock:IRCSend('JOIN '..newvalue)
						sock.Channel = newvalue
					end
				end
			end
		end,
				
		['reconnect'] = {
			['attempts'] = function(args)
				--test.print(tc{"cli.server.reconnect.attempts(",tc(args, ','),')'})
				local newvalue = tonumber(args[1])
				update_value(newvalue, irc.Reconnect, 'Attempts', 'Server.Reconnect.Attempts', 'server reconnect attempts <number>\nIf <number> is less than 0 then attempts are unlimited')
			end,

			['time'] = function(args)
				--test.print(tc{"cli.server.reconnect.time(",tc(args, ','),')'})
				local newvalue = tonumber(args[1])
				if (newvalue) then newvalue = math.abs(newvalue) end
				update_value(newvalue, irc.Reconnect, 'Time', 'Server.Reconnect.Time', 'server reconnect time <number>')
			end,

			['_default'] = function(cmd_branch, args)
				--test.print(tc{"cli.server.reconnect._default(",tc(args, ','),')'})
				return help(cmd_branch, args[1])
			end,

		},
-- 		['add'] = function(args)
-- 			--test.print(tc{"cli.server.add(",tc(args, ','),')'})
-- 		end,
-- 				
-- 		['change'] = function(args)
-- 			--test.print(tc{"cli.server.change(",tc(args, ','),')'})
-- 		end,
-- 				
-- 		['del'] = function(args)
-- 			--test.print(tc{"cli.server.del(",tc(args, ','),')'})
-- 		end,
-- 				
-- 		['list'] = function(args)
-- 			--test.print(tc{"cli.server.list(",tc(args, ','),')'})
-- 		end,
				
		['_default'] = function(cmd_branch, args)
			--test.print(tc{"cli.server._default(",tc(args, ','),')'})
			return help(cmd_branch, args[1])
		end,
	},
	
	['channels'] = {
		['default'] = function(args)
			--test.print(tc{"cli.channels.default(",tc(args, ','),')'})
			local newvalue = tonumber(args[1])
			update_value(newvalue, irc.VOChannels, 'Default', 'Channels.Default', 'channels default <integer>')
			
			-- Join the new default if not already so
			if (not irc.VOChannels.Details[newvalue]) then irc:JoinVOChannel{newvalue} end
		end,
		['rescan'] = function(args)
			--test.print(tc{"cli.channels.rescan(",tc(args, ','),')'})
		
		
		
		end,

 		['join'] = function(args)
 			--test.print("cli/channel/join")
 		end,
-- 				
 		['leave'] = function(args)
 			--test.print("cli/channel/leave")
 		end,
-- 		
-- 		['list'] = function(args)
-- 			--test.print("cli/channel/list")
-- 		end,
				
		['_default'] = function(cmd_branch, args)
			--test.print(tc{"cli.channel._default(",tc(args, ','),')'})
			return help(cmd_branch, args[1])
		end,
	},
	
	['data'] = {
		['load'] = function(args)
			--test.print(tc{"cli.data.load(",tc(args, ','),')'})
			irc.LoadData()
			print('\127ffffffData has been loaded.')

		end,
				
		['reset'] = function(args)
			--test.print(tc{"cli.data.reset(",tc(args, ','),')'})
			if (next(irc.Sockets)) then
				print('\127ffffffYou must disconnect all servers before resetting.')
			else
				irc.IRCNameData.Nicknames = {}
				irc.IRCNameData.Lookup = {}
				irc.VONameData.Characters = {}
				irc.VONameData.Lookup = {}
				irc.SaveData()
				print('\127ffffffData has been reset.')
			end
		end,
				
		['save'] = function(args)
			--test.print(tc{"cli.data.save(",tc(args, ','),')'})
				irc.SaveData()
				print('\127ffffffData has been saved.')
		end,
			
		['_default'] = function(cmd_branch, args)
			--test.print(tc{"cli.data._default(",tc(args, ','),')'})
			return help(cmd_branch, args[1])
		end,
	},
				
	['_default'] = function(cmd_branch, args)
		--test.print(tc{"cli._default(",tc(args, ','),')'})
		return help(cmd_branch, args[1])
	end,
}

local function process_cli(args, cmd_branch)

    if (not args[1]) then
		return cmd_branch._default(cmd_branch, args)
    else 
        local cmd_name = tr(args,1):lower()
		local cmd = cmd_branch[cmd_name]
		local t = type(cmd)
		if     (cmd and t == 'function') then
				return cmd(args)

		elseif (cmd and t == 'table') then
			return process_cli(args, cmd)

		else
			ti(args, 1, cmd_name)
			return cmd_branch._default(cmd_branch, args)
		end
    end
end	

local function cli(_, args)
	args = args or {}
--	local cmd_branch = shell_mode.branch or cli_commands
	local cmd_branch = cli_commands
	return process_cli(args, cmd_branch)
end

RegisterUserCommand(irc.CommandName, cli)