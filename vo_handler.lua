local tc, tr, ts = table.concat, table.remove, table.sort
local old_chatreciever

local function new_chatreceiver(self, event, ...)
	if (event == 'CHAT_MSG_PRIVATE' and irc.HidePMs) then return end
	if (old_chatreciever) then old_chatreciever(self, event, ...) end
end

local function ParseMessage(msg)
	local cmd, nmsg = msg:match('^(%S+) (.*)')
	cmd = cmd or msg
	return cmd:lower(), nmsg
end

local function process_chat(event, data)
	irc:DebugPrint('vo_handler.lua: process_chat()', 'II', 2, true)

	local chan, msg, name, faction = data.channelid, data.msg, data.name, data.faction
	local lname = name:lower()

	if (irc.VOChannels.Forbidden[chan]) then return end
	--if (name == GetPlayerName() and msg:match('^%[IRC%]')) then return end
	
	if (name == GetPlayerName()) then
		if (msg:match('^'..irc.VOMsgPrefix:gsub('(%p)', '%%%1')..'<.->') or
			msg:match('^'..irc.VOMsgPrefix:gsub('(%p)', '%%%1')..'* ')) then return end
	else
		if (irc.VONameData.Characters[lname]) then irc.VONameData:UpdateLastSeen(lname) end
	end
	
	local to_irc = true
	local emote = event:match('EMOTE')

	if (next(irc.Sockets)) then
		local prefix = irc:FormatName(name, chan, emote, faction, to_irc)
		irc:IRCBroadcast(prefix..msg)
	end
end

function irc:CHAT_MSG_CHANNEL(event, data) process_chat(event, data) end
function irc:CHAT_MSG_CHANNEL_EMOTE(event, data) process_chat(event, data) end
function irc:CHAT_MSG_CHANNEL_ACTIVE(event, data) process_chat(event, data) end
function irc:CHAT_MSG_CHANNEL_EMOTE_ACTIVE(event, data) process_chat(event, data) end

function irc:CHAT_MSG_GLOBAL_SERVER(event, data)
	local msg = data.msg
	if (msg ~= 'Your station is under attack!') then
		if (next(irc.Sockets)) then
			irc:IRCBroadcast('[VO] *** '..msg)
		end
	end
	
end

function irc:CHAT_MSG_PRIVATE(event, data)
	irc:DebugPrint('irc:CHAT_MSG_PRIVATE()', 'II', 2, true)

	local origin = data.name
	local lorigin = origin:lower()
	local cmd, nmsg = ParseMessage(data.msg)
	local cdata = irc.VONameData.Characters[lorigin]
	local autoreply

	if (cmd:match('^'..irc.NonRelayTrigger)) then return end
	
	if (cdata) then 
		self.VONameData:UpdateLastSeen(lorigin)
		autoreply = cdata.AutoReply
	end

	local msg
	if (irc.Commands[cmd]) then
		msg = nmsg

	elseif (autoreply) then
		cmd = 'msg'
		msg = '"'..autoreply..'" '..data.msg

	else
		-- SendChat('Unrecognized command: '..cmd, 'PRIVATE', name)
		return
	end

	data = {
		['source'] = 'VO',
		['origin'] = {
			['nickname'] = origin,
		},
		['msg'] = msg,
		['faction'] = data.faction,
	}

	local cl = irc.CommandLevels[cmd]
	if (not cl or cl < 100 or (cl == 100 and irc.VODevelopers[origin])) then
		console_print(cmd) -- I've been seeing errors pop up, lets get the command that's causing it.
		irc.Commands[cmd](nil, data)
	end
end

function irc:CHAT_MSG_SERVER(event, data)
	local lname = data.msg:match('*** (.*) is not online.')
	if (lname and irc.VOMsgQueue['PRIVATE'].Attempted[lname]) then
		local sock = irc.VOMsgQueue['PRIVATE'].Attempted[lname].Sock
		local nick = irc.VOMsgQueue['PRIVATE'].Attempted[lname].Nickname
		if (sock) then sock:IRCNotice(nick, data.msg) end
		irc.VOMsgQueue['PRIVATE'].Attempted[lname] = nil
	end
end

function irc:PLAYER_ENTERED_GAME(event, data)
	irc:DebugPrint('irc:PLAYER_ENTERED_GAME()', 'II', 2, true)

	-- Get channels
	local voc = self.VOChannels
	-- Clear previously connected channels if there is old data and re-read
	-- TODO I don't think i want to keep these three lines here like this, especially not the Max reset
	voc['Details'] = {}
	voc['List'] = {}
	voc['Max'] = gkini.ReadInt('irc_relay', 'VOChannels.Max', 32)

	local channels = {}
	for _, chan in ipairs(GetJoinedChannels()) do
		if (voc.Forbidden[chan]) then 
			voc.Max = voc.Max - 1
		else
			channels[#channels + 1] = chan
		end
	end
	irc:JoinVOChannel(channels, true)
	
	irc.VOConnected = true
	
	irc.LoadData()
	irc.SetCommandLevels()
	irc.SetCustomCommandLevels()

	
	if (irc.AutoStart) then gkinterface.GKProcessCommand('irc start') end
end

function irc:PLAYER_LOGGED_OUT(event, data)
	irc:DebugPrint('irc:PLAYER_LOGGED_OUT()', 'II', 2, true)
	
	irc.VOConnected = false
end

function irc:UNLOAD_INTERFACE(event, data)
	irc:DebugPrint('irc:UNLOAD_INTERFACE()', 'II', 2, true)

	gkinterface.GKProcessCommand('irc stop')
end

local chat_events = {
	'CHAT_MSG_CHANNEL',
	'CHAT_MSG_CHANNEL_EMOTE',
	'CHAT_MSG_CHANNEL_ACTIVE',
	'CHAT_MSG_CHANNEL_EMOTE_ACTIVE',
	'CHAT_MSG_GLOBAL_SERVER',
	'CHAT_MSG_PRIVATE',
}

local other_events = {
	'CHAT_MSG_SERVER',
	'PLAYER_ENTERED_GAME',
	'PLAYER_LOGGED_OUT',
	'UNLOAD_INTERFACE',
}
	
function irc:RegisterEvents(events)
	irc:DebugPrint('irc:RegisterEvents()', 'II', 2, true)
	for _, event in ipairs(events) do RegisterEvent(irc, event) end
end

function irc:UnregisterEvents(events)
	irc:DebugPrint('irc:UnregisterEvents()', 'II', 2, true)
	for _, event in ipairs(events) do UnregisterEvent(irc, event) end
end

function irc:VOReconfig()
	old_chatreciever = chatreceiver.OnEvent
	chatreceiver.OnEvent = new_chatreceiver
end

function irc:RegisterChatEvents() self:RegisterEvents(chat_events) end
function irc:UnregisterChatEvents() self:UnregisterEvents(chat_events) end

function irc.Cleanup.vo_handler()
	chatreceiver.OnEvent = old_chatreciever
	irc:UnregisterEvents(chat_events)
	irc:UnregisterEvents(other_events)
end

-- Register events if file reloaded while connected
if (next(irc.Sockets)) then
	irc:RegisterEvents(chat_events)
end
irc:RegisterEvents(other_events)

irc:VOReconfig()

--[[Event: CHAT_MSG_GLOBAL_SERVER - Data:
        1: (table)      msg="Server will restart in 1 minute: adding some metrics and fixing escorts"
]]