local tc, ts = table.concat, table.sort

--[[
Custom commands must be added to the table irc.Commands and the function name must be lowercase.

Commands will receive three arguments from the IRC PRIVMSG response
	connection -- a reference to the table identifying the source of the request
	data       -- a table containing the following structure
		{
			['source'] = <string>,      -- "IRC" or "VO",
			['origin'] = { 
				['nickname'] = <string>,-- the nickname of the request origin
				['channel'] = <string>, -- the channel of the request origin (public trigger)
			},
			['msg'] = <string>,         -- the remainder of the message after the command is stripped
			['private'] = <boolean>,    -- a boolean indicating whether the request was made via a private message or public trigger
		}

Commands can implement other arguments as necessary after these three.


irc:VOSend() reqires that the message table have the following structure.

	message_table = {
		msg = <string>,
		origin = <string>,     -- don't provide origin if the message is from the relay
		identity = <string>,   -- linked identity to use when sending the message
		dest = {
			<string>,          -- the VO chat channel (CHANNEL, PRIVATE, GUILD, etc).
			<string>||<number> -- the parameter for [1]
		},
		emote = <boolean>,     -- is this message an emote?
		echo = <boolean>,      -- should this message be echoed back to IRC after being sent to VO?
		sock = <table>,        -- the table representing the tcpsock this message is originating from
							-- optional if the message is from the relay
	}

connection:IRCChat() and :IRCNotice() take only two arguments
	destination
	message

If you want to send a custom IRC command, use connection:IRCSend(msg).  The IRC message must be valid and :IRCSend() will
append "\r\n" to the end of the message automatically.
]]

irc.Commands = irc.Commands or {}

irc.Commands['kill'] = function(sock, data)
	if (data.source == 'VO') then
		if (irc.KillSwitchACL[data.origin.nickname]) then
			gkinterface.GKProcessCommand('irc stop')
		end
	else
		data.msg = tc({'kill',data.msg}, ' ')
		irc.Commands['ch'](sock, data)
	end
end

irc.Commands['score'] = function(sock, data)
	irc.DebugPrint('irc.Commands.score()', 'II', 2, true)

	local origin = data.origin.nickname
	local conq = GetConqueredSectorsInSystem(4) or {}
	local itani, serco = 0, 0

	for k, v in pairs(conq) do
		if (v == 1) then itani = itani + 1
		elseif (v == 2) then serco = serco + 1
		end
	end
	
	local score = 'Border Skirmish Score -- Itani: '..itani..' / Serco: '..serco
	
	if (sock) then
		--if (data.private) then
			sock:IRCNotice(origin, score)
		--else
			--sock:IRCNotice(data.origin.channel, score)
		--end
	else
		irc:VOChatPrivate(origin, score)
	end
end

function irc.SetCustomCommandLevels()
	local cl = irc.CommandLevels or {}
	--cl['example'] = (irc.RequireLink and irc.RequireAuth and 4) or (irc.RequireAuth and 2) or 0
	irc.CommandLevels = cl
end