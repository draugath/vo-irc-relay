local tc, ti, tr, ts = table.concat, table.insert, table.remove, table.sort
local _

--[[
Commands will receive three arguments from the IRC PRIVMSG response
	connection -- a reference to the table identifying the source of the request
	data       -- a table containing the following structure
		{
			['source'] = <string>,      -- "IRC" or "VO",
			['origin'] = { 
				['nickname'] = <string>,-- the nickname of the request origin
				['channel'] = <string>, -- the channel of the request origin (public trigger)
			},
			['identity']                -- (optional) identity to use
			['msg'] = <string>,         -- the remainder of the message after the command is stripped
			['faction'] = <number>      -- VO only
			['private'] = <boolean>,    -- a boolean indicating whether the request was made via a private message or public trigger
		}

Commands can implement other arguments as necessary after these three.
]]

--[[
Messages sent to VO must be encapsulated in a table with the following format
message_table = {
	msg = <string>,
	origin = <string>,     -- don't provide origin if the message is from the relay
	identity = <string>,   -- (optional) linked identity to use when sending the message (defaults to origin)
	dest = {
		<string>,          -- the VO chat channel (CHANNEL, PRIVATE, GUILD, etc).
		<string>||<number> -- the parameter for [1]
	},
	emote = <boolean>,     -- is this message an emote?
	echo = <boolean>,      -- should this message be echoed back to IRC after being sent to VO?
	sock = <table>,        -- the table representing the tcpsock this message is originating from
	                       -- optional if the message is from the relay
}
]]

local function ParseMessage(msg)
	local cmd, nmsg = msg:match('^(%S+) (.*)')
	cmd = cmd or msg
	return cmd:lower(), nmsg
end

local function strip_name_quotes(name, strip)
	if (not name) then return '' end
	local stripped_str
	local found, stripped_name = name:match('^(")(.-)%1')
	if (not found and strip) then 
		stripped_name = name:match('^%S+')
	end

	if (strip) then
		if (not found) then
			stripped_str = name:gsub('^%S+%s*', '')
		else
			stripped_str = name:gsub('^(")(.-)%1%s*', '')
		end
	end
	return stripped_name or name, stripped_str
end

local function get_ignore_list(lname, address)
	local list = {}
	local data
	if (address) then
		data = irc.IRCNameData.Nicknames[address][lname]
		for n in pairs(data.Ignore) do list[#list + 1] = n end

	else
		data = irc.VONameData.Characters[lname]
		for n in pairs(data.Ignore) do list[#list + 1] = n end
	end
	ts(list)
	return list
end

irc.Commands = irc.Commands or {}

irc.Commands['as'] = function(sock, data)
	irc:DebugPrint('irc.Commands.as()', 'II', 2, true)

	if (data.source == 'IRC') then
		local origin = data.origin.nickname
		local lorigin = origin:lower()
		local name = data.msg
		local lname
		local msg, info_msg
		if (not name or name == '') then
			info_msg = 'as: You must specify a name.'

		else
			local ndata = irc.IRCNameData.Nicknames[sock.Address][lorigin]
			name, msg = strip_name_quotes(name, true)
			lname = name:lower()
			if (ndata.LinkedTo[lname]) then
				name = irc.VONameData.Characters[lname].ProperName
				if (msg and msg ~= '' and not msg:match('^as')) then
					local cmd, nmsg = ParseMessage(msg)
					if (cmd and irc.Commands[cmd]) then
						data.msg = nmsg
						data.identity = name
						irc.Commands[cmd](sock, data)

					else
						info_msg = 'as: invalid command'
					end

				else
					ndata.CurLink = name
					info_msg = 'Your current link has been changed to: '..name
				end

			else
				info_msg = 'You are not linked to "'..(name or lname)..'".'
			end
		end
		if (info_msg) then sock:IRCNotice(origin, info_msg) end
	end
end

irc.Commands['ban'] = function(sock, data)
	irc:DebugPrint('irc.Commands.ban()', 'II', 2, true)
	
	local origin = data.origin.nickname

	if (data.source == 'IRC') then

		if (data.msg) then
			local nick, duration, reason = data.msg:match('^(%S+)%s+(%d+)%s+(.*)')
			
			if (nick and duration and reason) then
				local lnick = nick:lower()
				local success, errmsg = irc.IRCNameData:Ban(sock.Address, lnick, duration, reason)
				if (success) then
					sock:IRCNotice(origin, irc.IRCNameData.Nicknames[sock.Address][lnick].ProperName..' has been banned for '..tostring(duration)..' seconds.')
				else
					sock:IRCNotice(origin, errmsg)
				end
				
			else
				sock:IRCNotice(origin, 'Invalid syntax.  Please check your arguments and try again.')
			end

		else
			sock:IRCNotice(origin, 'Invalid syntax.  Please check your arguments and try again.')
		end

	elseif (data.source == 'VO') then
		if (data.msg) then 
			local name, nmsg = strip_name_quotes(data.msg, true)
			local duration, reason = nmsg:match('^(%d+)%s+(.*)')
			duration = tonumber(duration)

			if (name and duration and reason) then
				local lname = name:lower()
				local cdata = irc.VONameData.Characters[lname]

				if (cdata and next(cdata.LinkedTo)) then
					for server, lnick in pairs(cdata.LinkedTo) do
						local success, errmsg = irc.IRCNameData:Ban(server, lnick, duration, reason)
						if (success) then
							irc:VOChatPrivate(origin, 'The user behind '..cdata.ProperName..' has been banned from the relay for '..tostring(duration)..' seconds.')
						else
							irc:VOChatPrivate(origin, errmsg)
						end
					end

				else
					irc:VOChatPrivate(origin, 'Invalid name or name is not linked to an IRC nickname.')
				end
				
			else
				irc:VOChatPrivate(origin, 'Invalid syntax. Please check your arguments and try again.')
			end
			
		else
			irc:VOChatPrivate(origin, 'Invalid syntax.  Please check your arguments and try again.')
		end
		
	end
end

irc.Commands['ch'] = function(sock, data, dest)
	irc:DebugPrint('irc.Commands.ch()', 'II', 2, true)

	if (data.source == 'IRC') then
		local origin = data.origin.nickname
		local curlink = data.identity or irc.IRCNameData.Nicknames[sock.Address][origin:lower()].CurLink
		local msg, emote = data.msg
		
		if (not curlink and irc.RequireLink) then
			sock:IRCNotice(origin, 'You need to specify your default identity with the "as" command.')
		elseif (curlink and irc.VONameData.Characters[curlink:lower()].LastSeen + (irc.ExpireLink * irc.ExpireLinkMultiplier) < os.time()) then
			sock:IRCNotice(origin, 'This character link has expired.  Please logon as '..curlink..' and send a message to '..GetPlayerName()..' to refresh the link.')
		else

			dest = dest or irc.VOChannels.Default
			msg, emote = msg:gsub('^/me ', '')
			emote = emote ~= 0 and true

			local msg_table = {
				['msg'] = msg,
				['origin'] = origin,
				['identity'] = curlink,
				['dest'] = {'CHANNEL', dest},
				['emote'] = emote,
				['echo'] = data.private or irc.EchoPublic,
				['sock'] = sock,
			}
			irc:VOSend(msg_table)
		end
	end
end
	
irc.Commands['help'] = function(sock, data)
	irc:DebugPrint('irc.Commands.help()', 'II', 2, true)

	local origin = data.origin.nickname
	local source = data.source
	
	local word = data.msg and data.msg:match('%w+')
	if (word and word:match('^[^_]')) then
		if (not irc.Help[source][word]) then
			word = '_nohelp'
		end
	else
		word = '_default'
	end
	
	if (irc.Help[source][word]) then
		for _, line in ipairs(irc.Help[source][word]) do
			if (source == 'IRC') then
				sock:IRCNotice(origin, line)
			elseif (source == 'VO') then
				irc:VOChatPrivate(origin, line)
			end
		end
	end
end

irc.Commands['ignore'] = function(sock, data)
	irc:DebugPrint('irc.Commands.ignore()', 'II', 2, true)
	
	local origin = data.origin.nickname
	local lorigin = origin:lower()
	local name = strip_name_quotes(data.msg)
	local lname = name:lower()
	local info_msg
	
	if (data.source == 'IRC') then
		if (not name or name == '') then
			info_msg = 'Ignore list: '..tc(get_ignore_list(lorigin, sock.Address), ', ')

		else
			irc.IRCNameData.Nicknames[sock.Address][lorigin].Ignore[lname] = true
			info_msg = 'Ignoring '..name
		end
		sock:IRCNotice(origin, info_msg)
		
	elseif (data.source == 'VO') then
		if (not name or name == '') then
			info_msg = 'Ignore list: '..tc(get_ignore_list(lorigin), ', ')

		else
			if (not irc.VONameData.Characters[lorigin]) then
				irc.VONameData.Characters[lorigin] = {['Ignore'] = {[lname] = true}}

			else
				irc.VONameData.Characters[lorigin].Ignore[lname] = true
			end
			info_msg = 'Ignoring '..name
		end
		irc:VOChatPrivate(origin, info_msg)
	end
end

irc.Commands['join'] = function(sock, data)
	irc:DebugPrint('irc.Commands.join()', 'II', 2, true)

	local origin = data.origin.nickname

	if (data.source == 'IRC') then
		local msg = data.msg
		local vochannel = tonumber(msg:match('^[ex+%d]+'))
		local irc_chan = msg:match('^%d+ [Ii][Nn] (#%S+)')
		
		if (vochannel and vochannel > 0 and vochannel ~= 'inf') then
			local voc = irc.VOChannels
			if (not voc.Forbidden[vochannel]) then
				if (voc.Count < voc.Max) then
					if (voc.Details[vochannel]) then
						sock:IRCNotice(origin, 'Already joined')
					else
						irc:JoinVOChannel{vochannel}
						msg = 'Joined channel: '..vochannel
						--sock:IRCNotice(origin, msg)
						irc:IRCBroadcast(msg, true) --sock:IRCChat(irc.channel, msg)
					end
				else
					sock:IRCNotice(origin, 'Already listening on the maximum number of channels.')
				end
			else
				sock:IRCNotice(origin, 'Joining that channel ('..vochannel..') is not allowed.')
			end
		else
			sock:IRCNotice(origin, 'Invalid channel')
		end
	end
end

irc.Commands['leave'] = function(sock, data)
	irc:DebugPrint('irc.Commands.leave()', 'II', 2, true)

	local origin = data.origin.nickname

	if (data.source == 'IRC') then
		local msg = data.msg
		local vochannel = tonumber(msg:match('^[ex+%d]+'))
		local irc_chan = msg:match('^%d+ [Ii][Nn] (#%S+)')

		if (vochannel) then
			local voc = irc.VOChannels
			if (voc.Details[vochannel]) then
				if (voc.Default == vochannel) then
					sock:IRCNotice(origin, 'Not allowed to leave the default channel.')
				else
					irc:LeaveVOChannel{vochannel}
					msg = 'Leaving channel: '..vochannel
					--sock:IRCChat(origin, msg)
					irc:IRCBroadcast(msg, true) --sock:IRCChat(irc.channel, msg)
				end
			else
				sock:IRCNotice(origin, 'Not joined to channel')
			end
		else
			sock:IRCNotice(origin, 'Invalid channel')
		end
	end
end

irc.Commands['link'] = function(sock, data)
	irc:DebugPrint('irc.Commands.link()', 'II', 2, true)
	-- linking shall be two-part process starting with IRC
	-- I might give some thought later to allowing it to start from VO
	local origin = data.origin.nickname
	local lorigin = origin:lower()
	local name = strip_name_quotes(data.msg)
	local lname
	local cdata
	local msg

	if (data.source == 'IRC') then
		if (not name or name == '') then
			msg = 'link: You must provide a name.'

		else
			lname = name:lower()
			cdata = irc.VONameData.Characters[lname]
			if (not cdata) then
				cdata = {['PendingLinks'] = {}}
				irc.VONameData.Characters[lname] = cdata
			end
			
			if (cdata.LinkedTo and next(cdata.LinkedTo)) then
				msg = 'The character '..name..' is already linked to another IRC nickname.'

			else
				cdata.PendingLinks = cdata.PendingLinks or {}
				cdata.PendingLinks[origin:lower()] = sock.Address
				msg = 'Now you need to login to the game as '..name..' and use the command: /msg '..GetPlayerName()..' link '..origin
			end
		end
		sock:IRCNotice(origin, msg)
		
	elseif (data.source == 'VO') then
		
		if (not name or name == '') then
			msg = 'link: You must provide a name.'
		
		else
			lname = name:lower()

			cdata = irc.VONameData.Characters[lorigin]
			if (cdata) then
				if (cdata.PendingLinks and next(cdata.PendingLinks)) then
					if (cdata.PendingLinks[lname]) then
						local server = cdata.PendingLinks[lname]

						cdata.ProperName = origin
						cdata.LastSeen = os.time()
						cdata.LinkedTo = {[server] = name}
						cdata.Faction = data.faction
						cdata.Ignore = cdata.Ignore or {}

						cdata.PendingLinks = nil

						local ndata = irc.IRCNameData.Nicknames[server][lname]
						ndata.LinkedTo = ndata.LinkedTo or {}
						ndata.LinkedTo[lorigin] = true
						ndata.CurLink = origin
						
						irc.Sockets[server].Auth.STATUS[name] = 4

						msg = 'Link complete. You can now talk as '..origin..' from IRC.'

					else
						msg = 'You need to initiate the link process from IRC first.' --  /msg '..irc.Nickname..' link '..origin
					end

				else
					msg = 'This character is already linked to an IRC nickname.'
				end

			else
				msg = 'You need to initiate the link process from IRC first.' --  /msg '..irc.Nickname..' link '..origin
			end
		end
		irc:VOChatPrivate(origin, msg)
	end
end

irc.Commands['links'] = function(sock, data)
	irc:DebugPrint('irc.Commands.links()', 'II', 2, true)

	local origin = data.origin.nickname
	local linked_list = {}
	
	if (data.source == 'IRC') then
		local ndata = irc.IRCNameData.Nicknames[sock.Address][origin:lower()]
		local cdata = irc.VONameData.Characters
		for name in pairs(ndata.LinkedTo) do
			name = cdata[name].ProperName
			if (ndata.CurLink == name) then name = '>'..name..'<' end
			ti(linked_list, name)
		end
		ts(linked_list)
		sock:IRCNotice(origin, 'You are linked to the following VO character'..(#linked_list > 1 and 's' or '')..': '..tc(linked_list, ', '))

	elseif (data.source == 'VO') then
		local cdata = irc.VONameData.Characters[origin:lower()]
		if (cdata) then
			if (cdata.LinkedTo) then
				for server, nick in pairs(cdata.LinkedTo) do
					ti(linked_list, irc.IRCNameData.Nicknames[server][nick].ProperName..'@'..server)
				end
			end
			ts(linked_list)
			irc:VOChatPrivate(origin, 'You are linked to the following IRC nickname'..(#linked_list > 1 and 's' or '')..': '..tc(linked_list, ', '))
		end
	end
end

irc.Commands['list'] = function(sock, data)
	irc:DebugPrint('irc.Commands.list()', 'II', 2, true)

	local origin = data.origin.nickname

	local voc = irc.VOChannels
	local list = tc(voc.List, ', '):gsub(voc.Default..',', tc{'[',voc.Default,'],'})
	sock:IRCNotice(origin, 'Joined channels [ '..voc.Count..' / '..voc.Max..' ]: '..list)
end

irc.Commands['msg'] = function(sock, data)
	irc:DebugPrint('irc.Commands.msg()', 'II', 2, true)

	local origin = data.origin.nickname
	local lorigin = origin:lower()
	local name, msg = strip_name_quotes(data.msg, true)
	local lname = name:lower()

	--test.varprint(' / ', 'origin', origin, 'name', name, 'data', data)
	if (data.source == 'IRC') then
		if (msg and msg ~= '') then
			--test.varprint(' / ', 'lorigin', lorigin, 'nick', irc.Nickname:lower())
			if (lorigin == irc.Nickname:lower()) then 
				console_print('----no "msg" from the relay')
				return 
			elseif (lname == GetPlayerName():lower()) then
				console_print('----no "msg" to the relay')
				return 
			end
			local curlink = data.identity or irc.IRCNameData.Nicknames[sock.Address][lorigin].CurLink
			local cdata = irc.VONameData.Characters[lname]
			if (not cdata or (cdata and cdata.Ignore and not cdata.Ignore[curlink:lower()])) then
				local msg_table = {
					['msg'] = msg,
					['origin'] = origin,
					['identity'] = curlink,
					['dest'] = {'PRIVATE', lname,},
					['sock'] = sock,
				}
				irc:VOSend(msg_table)

				if (not cdata) then 
					cdata = {['Ignore'] = {},}
					irc.VONameData.Characters[lname] = cdata
				end
				--test.varprint(' / ', 'cdata', cdata)
				if (cdata.AutoReply ~= curlink) then
					cdata.AutoReply = curlink
					irc:VOChatPrivate(lname, 'Auto-reply set to '..curlink..'.')
				end
			end
			
		else
			sock:IRCNotice(origin, 'msg: You must include a message.')
		end
			
	elseif (data.source == 'VO') then
		if (msg and msg ~= '') then
			--test.varprint(' / ', 'lorigin', lorigin, 'nick', GetPlayerName():lower())
			if (lorigin == GetPlayerName():lower()) then 
				console_print('----no "msg" from the relay')
				return
			elseif (lname == irc.Nickname:lower()) then 
				console_print('----no "msg" to the relay')
				return
			end

			local cdata = irc.VONameData.Characters[lname]
			if (cdata and cdata.LinkedTo) then
				name = cdata.ProperName
				local count = 0
				
				for address, nick in pairs(cdata.LinkedTo) do
					local lnick = nick:lower()
					sock = irc.Sockets[address]
					local ndata = irc.IRCNameData.Nicknames[address][lnick] 
					local nick = ndata.ProperName

					if (sock.Auth.STATUS[nick] == 4) then
						if (ndata.Ignore and not ndata.Ignore[lorigin]) then
							if (ndata.CurLink ~= name) then msg = ' -> '..name..': '..msg end
							sock:IRCChat(nick, irc:FormatName(origin, nil, nil, data.faction, nil, nil, true)..msg)

							--test.varprint(' / ', 'origin', origin, 'name', name, 'curlink', ndata.CurLink, 'AR', ndata.AutoReply)
							--if (ndata.AutoReply) then test.print(tostring(not ndata.AutoReply), tostring(ndata.AutoReply.Dest ~= origin), tostring(ndata.AutoReply.As ~= name), (ndata.AutoReply and ndata.AutoReply.Dest ~= origin or ndata.AutoReply.As ~= name)) end
							if (not ndata.AutoReply or (ndata.AutoReply and ndata.AutoReply.Dest ~= origin or ndata.AutoReply.As ~= name)) then
								ndata.AutoReply = {['Dest'] = origin, ['As'] = name}
								--if (ndata.CurLink ~= name) then ndata.AutoReply.As = name end

								sock:IRCChat(nick, 'Auto-reply set to '..origin..'.  You will be replying as '..name..'.')
							end
						end

						count = count + 1
					end
				end

				if (count == 0) then
					irc:VOChatPrivate(origin, '*** '..lname..' is not online.')
				end
			
			else
				irc:VOChatPrivate(origin, '*** '..lname..' is not online.')
			end

		else
			irc:VOChatPrivate(origin, 'msg: You must include a message.')
		end
	end
end
	
irc.Commands['mute'] = function(sock, data)
	irc:DebugPrint('irc.Commands.mute()', 'II', 2, true)
	
	if (sock) then 
		data.msg = 'mute '..data.msg
		irc.Commands['ch'](sock, data) 
	else
		local mutelist = irc.MuteList
		local nick = data.msg
		local ndata = irc.NameData.IRC[nick]
		if (ndata) then
			local now = os.time()
			mutelist[nick] = mutelist[nick] or {Count = 0, Start = now}
			m = mutelist[nick]
			if (m.Start + 900 < now) then 
				m.Start = now
				m.Count = 0
			end
			m.Count = m.Count + 1
			if (m.Count > 6) then 
				if (ndata.Ban and ndata.Ban.Time < now + 900) then
					local ban = {['Time'] = now + 900, ['Reason'] = 'Muted'}
					ndata.Ban = ban
				end
			end
		end
	end
end

irc.Commands['noreply'] = function(sock, data)
	irc:DebugPrint('irc.Commands.noreply()', 'II', 2, true)

	local origin = data.origin.nickname
	local lorigin = origin:lower()
	
	if (data.source == 'IRC') then
		irc.IRCNameData.Nicknames[sock.Address][lorigin].AutoReply = nil
		sock:IRCNotice(origin, 'Auto-reply cleared.')
		
	elseif (data.source == 'VO') then
		local cdata = irc.VONameData.Characters[lorigin]
		if (cdata) then
			cdata.AutoReply = nil
		end
		irc:VOChatPrivate(origin, 'Auto-reply cleared.')
	end
end




irc.Commands['toch'] = function(sock, data)
	irc:DebugPrint('irc.Commands.toch()', 'II', 2, true)

	local private = data.private
	if (not private) then return end

	local dest, nmsg = data.msg:match('^([ex+%d]+) (.*)')
	dest = tonumber(dest)

	if (dest) then
		local voc = irc.VOChannels
		if (voc.Forbidden[dest] or not voc.Details[dest]) then dest = nil else data.msg = nmsg end
	end
	irc.Commands.ch(sock, data, dest)
end

irc.Commands['unignore'] = function(sock, data)
	irc:DebugPrint('irc.Commands.unignore()', 'II', 2, true)
	
	local origin = data.origin.nickname
	local lorigin = origin:lower()
	local name = strip_name_quotes(data.msg)
	local lname = name:lower()
	local info_msg
	
	if (data.source == 'IRC') then
		if (not name or name == '') then
			info_msg = 'Ignore list: '..tc(get_ignore_list(lorigin, sock.Address), ', ')

		else
			local ndata = irc.IRCNameData.Nicknames[sock.Address][lorigin]
			if (ndata.Ignore) then ndata.Ignore[lname] = nil end
			info_msg = 'Unignoring '..name
		end
		sock:IRCNotice(origin, info_msg)
		
	elseif (data.source == 'VO') then
		if (not name or name == '') then
			info_msg = 'Ignore list: '..tc(get_ignore_list(lorigin), ', ')

		else
			local cdata = irc.VONameData.Characters[lorigin]
			if (cdata and cdata.Ignore) then
				cdata.Ignore[lname] = nil
			end
			info_msg = 'Unignoring '..name
		end
		irc:VOChatPrivate(origin, info_msg)
	end
end

irc.Commands['unlink'] = function(sock, data)
	irc:DebugPrint('irc.Commands.unlink()', 'II', 2, true)
	
	local origin = data.origin.nickname
	local lorigin = origin:lower()
	local name = strip_name_quotes(data.msg)
	local lname = name:lower()
	local ndata
	local cdata
	local info_msg

	if (data.source == 'IRC') then
		if (not name or name == '') then
			info_msg = 'link: You need to specify a name.'

		else
			name = irc.VONameData.Characters[lname].ProperName
			
			ndata = irc.IRCNameData.Nicknames[sock.Address][lorigin]
			if (ndata.LinkedTo and ndata.LinkedTo[lname]) then
				ndata.LinkedTo[lname] = nil
				if (ndata.CurLink == name) then ndata.CurLink = nil end
				if (not next(ndata.LinkedTo)) then sock.Auth.STATUS[origin] = 3 end

				cdata = irc.VONameData.Characters[lname]
				cdata.LinkedTo[sock.Address] = nil
				if (not next(cdata.LinkedTo) and not next(cdata.Ignore)) then
					irc.VONameData.Characters[lname] = nil
				end
				
				info_msg = 'You are no longer linked to the VO character "'..name..'".'
				
			else
				info_msg = 'You are not linked to that VO character.'
			end
		end
		sock:IRCNotice(origin, info_msg)

	elseif (data.source == 'VO') then
		cdata = irc.VONameData.Characters[lorigin]
		if (cdata and next(cdata.LinkedTo)) then
			for server, lname in pairs(cdata.LinkedTo) do
				ndata = irc.IRCNameData.Nicknames[server][lname]
				ndata.LinkedTo[lorigin] = nil
				name = ndata.ProperName
				if (ndata.CurLink == origin) then ndata.CurLink = nil end
				if (not next(ndata.LinkedTo)) then irc.Sockets[server].Auth.STATUS[name] = 3 end
			end

			cdata.LinkedTo = {}
			if (not next(cdata.LinkedTo) and not next(cdata.Ignore)) then irc.VONameData.Characters[lorigin] = nil end
			
			info_msg = 'This character is no longer linked to an IRC nickname.'
			
		else
			info_msg = 'This character is not linked to an IRC nickname.'
		end
		irc:VOChatPrivate(origin, info_msg)
	end
	
end

function irc.SetCommandLevels()
	local cl = irc.CommandLevels or {}
	cl['as'] = 4
	cl['ban'] = 100
	cl['ch'] = (irc.RequireLink and irc.RequireAuth and 4) or (irc.RequireAuth and 2) or 0
	cl['help'] = 0
	cl['ignore'] = 4
	cl['join'] = (irc.RequireLink and irc.RequireAuth and 4) or (irc.RequireAuth and 2) or 0
	cl['leave'] = (irc.RequireLink and irc.RequireAuth and 4) or (irc.RequireAuth and 2) or 0
	cl['link'] = 2
	cl['links'] = 4
	cl['msg'] = 4
	cl['noreply'] = 4
	cl['toch'] = (irc.RequireLink and irc.RequireAuth and 4) or (irc.RequireAuth and 2) or 0
	cl['unlink'] = 4
	cl['unignore'] = 4
	irc.CommandLevels = cl
end

irc.SetCommandLevels()