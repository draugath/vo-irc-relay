local tc, ti, tr, ts = table.concat, table.insert, table.remove, table.sort

function irc:IRCBroadcast(msg, notice, except)
	irc:DebugPrint('irc:IRCBroadcast()', 'II', 2, true)
	if (type(msg) == 'table') then msg = tc(msg) end
	for server, sock in pairs(self.Sockets) do
		if (sock ~= except and sock.Connected) then 
			local dest = sock.Channel
			if (notice) then
				sock:IRCNotice(dest, msg)
			else
				sock:IRCChat(dest, msg)
			end
		end
	end
end

function irc:VOSend(m)
	irc:DebugPrint('irc:VOSend()', 'II', 2, true)

	irc.VOMsgQueue:Enqueue(m)
end

function irc:VOChatPrivate(dest, msg)
	irc:DebugPrint('irc:VOChatPrivate()', 'II', 2, true)
	
	irc:VOSend({
		['msg'] = msg,
		['dest'] = {'PRIVATE', dest},
	})
end

function irc:VOChatChannel(dest, msg)
	
end

local function process_auth_queue(sock)
	irc:Authenticate(sock)
	if (#sock.Auth.Pending > 0) then
		sock.Auth.Timer:SetTimeout(1000, function() return process_auth_queue(sock) end)
	end
end

function irc:QueueCmdForReauth(sock, nick, action)
	irc:DebugPrint('irc:QueueCmdForReauth()', 'II', 2, true)
	sock.Auth.PendingCmd[nick] = sock.Auth.PendingCmd[nick] or {}
	ti(sock.Auth.PendingCmd[nick], action)
end

function irc:QueueNameForAuth(sock, nick, reason, action)
	irc:DebugPrint('irc:QueueNameForAuth()', 'II', 2, true)

	if (not irc.RequireAuth) then return end
	
	if (sock.Auth.Reason[nick]) then 
		if (reason == 'REAUTH') then
			return -- TODO figure out an easy to way make them wait for spamming the bot
		else
			return  -- Already queued, Just wait a moment.
		end
	end
	
	local delay = irc.AuthDelay

	if (reason == 'REAUTH') then 
		delay = irc.ReauthDelay
		sock.Auth.PendingCmd[nick] = sock.Auth.PendingCmd[nick] or {}
		ti(sock.Auth.PendingCmd[nick], action)
		sock.Auth.STATUS[nick] = nil -- This will prevent further commands and inform them of reauth.
	end

	if (reason == 353) then
		ti(sock.Auth.Pending[0], nick)
	else
		local count = #sock.Auth.Pending
		local diff = delay - count
		if (diff < 1) then
			ti(sock.Auth.Pending[delay], nick)
		elseif (diff == 1) then
			sock.Auth.Pending[delay] = {'STATUS', nick}
		else
			for i=count + 1, delay - 1 do
				sock.Auth.Pending[i] = {'STATUS'}
			end
			sock.Auth.Pending[delay] = {'STATUS', nick}
		end
	end
	
	--ti(sock.Auth.Pending, nick)
	sock.Auth.Reason[nick] = reason or 'unknown'
	
	-- Only start the timer if the relay is not currently joining a channel.
	if (reason == 353) then 
		if (#sock.Auth.Pending[0] == 17) then self:Authenticate(sock, 0) end
	else
		if (not sock.Auth.Timer:IsActive()) then 
			sock.Auth.Timer:SetTimeout(1000, function() return process_auth_queue(sock) end )
		end
	end
end

function irc:Authenticate(sock, index)
	irc:DebugPrint('irc:Authenticate()', 'II', 2, true)

	index = index or 1
	local nicklist
	if (index == 0) then
		nicklist = sock.Auth.Pending[0]
		sock.Auth.Pending[0] = {'STATUS'}
	else
		nicklist = tr(sock.Auth.Pending, index)
	end
	
	if (#nicklist > 1) then sock:IRCChat('NickServ', tc(nicklist, ' ')) end -- TODO fix nicklist to no more than 16 names.
end

function irc:JoinVOChannel(channels, init)
	irc:DebugPrint('irc:JoinVOChannel()', 'II', 2, true)

	local voc = self.VOChannels
	for _, chan in ipairs(channels) do
		voc.List[#voc.List+1] = chan
		voc.Details[chan] = {}
	end
	if (init) then 
		if (not voc.Details[voc.Default]) then
			self:JoinVOChannel{voc.Default}
		end
	else
		JoinChannel(channels)
	end
	voc.Count = #voc.List
	ts(voc.List)
end

function irc:LeaveVOChannel(channels)
	irc:DebugPrint('irc:LeaveVOChannel()', 'II', 2, true)

	local voc = self.VOChannels
	local chan_idx
	for _, chan_to_remove in ipairs(channels) do
		for i, c in ipairs(voc.List) do
			if (chan_to_remove == c) then
				chan_idx = i
				break
			end
		end
		tr(voc.List, chan_idx)
		voc.Details[chan_to_remove] = nil
	end
	LeaveChannel(channels)
	ts(voc)
end

function irc.IRCNameData:Initialize(server, nick)
	irc:DebugPrint('irc.IRCNameData:Initialize()', 'II', 2, true)
	-- Should not call this function unless the name has been verified as registered
	-- nick should be natural case
	local nd = self.Nicknames[server]

	nd[nick:lower()] = {
		['ProperName'] = nick,
		['FloodQueue'] = {},
		['Ignore'] = {},
		['LinkedTo'] = {},
	}
end
	
function irc.IRCNameData:Ban(server, nick, duration, reason)
	irc:DebugPrint('irc.IRCNameData:Ban()', 'II', 2, true)

	local ndata = self.Nicknames[server] and self.Nicknames[server][nick]
	local duration = tonumber(duration)
	local reason = tostring(reason)

	if (ndata) then
		ndata.Ban = {
			['Time'] = os.time() + duration,
			['Reason'] = reason,
		}
	else
		return false, 'Invalid nickname.'
	end
	return true
end

function irc.IRCNameData:IsBanned(server, nick)
	irc:DebugPrint('irc.IRCNameData:IsBanned()', 'II', 2, true)
	-- nick should be lowercase when passed to the function
	local remaining, reason
	local fc = irc.FloodControl
	if (fc.Enabled) then

		local ndata = self.Nicknames[server][nick]
		local ban, queue = ndata.Ban, ndata.FloodQueue
		local flood_ban_length = fc.BanLength
		local time = os.time()

		if (ban) then
			if (ban.Time < time) then
				ndata.Ban = nil
			else
				remaining = os.difftime(ban.Time, time)
				reason = ban.Reason
			end
		end

		if (not remaining) then
			while (#queue ~= 0) do
				if (queue[1] + fc.Period < time) then
					tr(queue, 1)
				else
					if (#queue > fc.Count) then
						remaining = flood_ban_length
						reason = 'Flooding'
						self:Ban(server, nick, flood_ban_length, reason)
					else
						remaining = nil
					end
					break
				end
			end
		end

	end
	return remaining, reason
end

function irc.IRCNameData:IncrementFloodCount(server, nick)
	irc:DebugPrint('irc.IRCNameData:IncrementFloodCount()', 'II', 2, true)
	-- nick should be lowercase when passed to the function
	if (irc.FloodControl.Enabled) then
		local queue = self.Nicknames[server][nick].FloodQueue
		queue[#queue+1] = os.time()
	end
end

function irc.VONameData:UpdateLastSeen(name)
	self.Characters[name:lower()].LastSeen = os.time()
end

function irc.LoadData()
	irc:DebugPrint('irc.LoadData()', 'II', 2, true)
	local ndata = unspickle(LoadSystemNotes(irc.SystemNotesID))
	for k, v in pairs(ndata) do irc.IRCNameData[k] = v end

	local cdata = unspickle(LoadSystemNotes(irc.SystemNotesID + 1))
	for k, v in pairs(cdata) do irc.VONameData[k] = v end
end

function irc.SaveData()
	irc:DebugPrint('irc.SaveData()', 'II', 2, true)
	local ndata = {['Nicknames'] = irc.IRCNameData.Nicknames,}-- ['Lookup'] = irc.IRCNameData.Lookup}
	SaveSystemNotes(spickle(ndata), irc.SystemNotesID)
	
	local cdata = {['Characters'] = irc.VONameData.Characters,}-- ['Lookup'] = irc.VONameData.Lookup}
	SaveSystemNotes(spickle(cdata), irc.SystemNotesID + 1)

	irc.SaveDataTimer:SetTimeout(irc.SaveDataDelay * 1000, irc.SaveData)
end

function irc:DebugPrint(line, icon, level, console_only)
	level = level or 1
	if (not self.Debug or level > self.Debug) then return end
	line = line:gsub('[^%w%p%s]', '')
	icon = icon or '>>'
	line = string.format("%s[%sirc\127o] %s%s %s%s\127o", "\127DD3333", "\127dd6666", "\127FFFFFF", icon, "\12777FF55", line)
	if (IsConnected() and not console_only) then print(line) else console_print(line) end
end

local sc = string.char
local r = sc(3) -- IRC color code modifier
local c = { -- irc colors
	[-1]=r.."10", -- teal
	[0]=r.."15", -- silver/grey
	[1]=r.."12", -- itani (blue)
	[2]=r.."04", -- serco (red)
	[3]=r.."07", -- orange/yellow (uit)
	b=sc(2), -- bold
	n=sc(15), -- normal
	purple=r.."06",
	yellow=r.."08",
	white=r.."00",
	black=r.."01",
	blue=r.."02", -- dark blue
	pink=r.."13",
	aqua=r.."11",
	green=r.."09", -- green that hurts your eyes...
	grey=r.."14",
}
local nwrap = { -- Fancy name ends
	['I'] = { -- IRC
		['L'] = {
			[true] = '',
			[false] = c[-1]..'<',
		},
		['R'] = {
			[true] = c[-1]..' ',
			[false] = c[-1]..'> ',
		},
	},
	['V'] = { -- VO
		['L'] = {
			[true] = '* ',
			[false] = '<',
		},
		['R'] = {
			[true] = ' ',
			[false] = c['n']..'> ',
		},
	},
	['P'] = { -- IRC-bound PMs
		['L'] = {
			[false] = '*',
		},
		['R'] = {
			[false] = c['n']..'* ',
		},
	},
}
--<vo>    10[100] 10<12Sarvok10> internet weather? wtf is internet weather
--<vo>    10[100] 10<15IRC1012draugath10> Incarnate verified

function irc:FormatName(name, chan, emote, faction, to_irc, from_irc, pm)
	irc:DebugPrint('irc:FormatName()', 'II', 2, true)

	if (not name) then
		if (to_irc) then return nil else return '' end
	end
	emote = emote and true or false
	faction = faction or 'n' --Normal coloring if no specified faction
	local prefix
	if (to_irc) then
		local prename = from_irc and c[0]..'IRC' or ''
		prefix = c[-1]..'['..chan..'] '..nwrap.I.L[emote]..prename..c[faction]..name..nwrap.I.R[emote]
	elseif (pm) then
		prefix = nwrap.P.L[emote]..c[faction]..name..nwrap.P.R[emote]
	else
		prefix = irc.VOMsgPrefix..nwrap.V.L[emote]..c[faction]..name..nwrap.V.R[emote]
	end	

	if (irc.VODevelopers[name]) then prefix = prefix:gsub('[<>]', '#') end

	return prefix
end
