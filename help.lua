irc.Help = {
	['IRC'] = {
-- 		['help'] = {
-- 			'Start of Help ----------------------------------------------------------',
-- 			'The following commands can be used with the relay:',
-- 			' ',
-- 			'    as      Change your current identity',
-- 			'    ch      Send a message to the default channel',
-- 			'    help      Display this help message.',
-- 			'    ignore      Ignore PMs received from the specified name',
-- 			'    join      Join an in-game channel,',
-- 			'    leave      Leave an in-game channel',
-- 			'    link      Link your nickname with a VO character',
-- 			'    links      Display your current list of character links',
-- 			'    list      List the current in-game channels being listened to',
-- 			'    msg      Send a PM to the specified name',
-- 			'    noreply      Clear your auto-reply destination',
-- 			'    score      Display the current status of conquered sectors in Deneb',
-- 			'    toch      Send a message to a channel other than the default channel',
-- 			'    unignore      Remove a name from your ignore list',
-- 			'    unlink      Remove a character link',
-- 			' ',
-- 			'End of Help ----------------------------------------------------------',
-- 		},
			
		['as'] = {
			'Change your default identity to the specified link.',
			'If you follow the name with a command, the identity will only be changed for that command.',
			'Syntax: as "<character name>"[ <command> ...]',
		},
		
		['ban'] = {
			'Ban the specified user from using the relay for the given number of seconds.',
			'Syntax: ban <nickname> <seconds> <reason>',
		},
		
		['ch'] = {
			'Send a message to the default channel.',
			'Syntax: ch <message>',
		},

		['ignore'] = {
			'Ignore private messages received from the specified name, or display current ignore list.',
			'Syntax: ignore [["]<character name>["]]',
		},

		['join'] = {
			'Join the relay to an in-game channel.',
			'Syntax: join <channel number>',
		},
		
		['leave'] = {
			'Leave an in-game channel.',
			'Syntax: leave <channel number>',
		},
		
		['link'] = {
			'Link your nickname with a VO character.  This character will then be used to identify you when using the relay.',
			'You can link more than one character to your nickname.',
			'Syntax: link <character name>',
		},
			
		['links'] = {
			'Display the list of links associated with your nickname.',
			'Syntax: links',
		},
		
		['list'] = {
			'List the current in-game channels being listened to.',
			'Syntax: list',
		},
		
		['msg'] = {
			'Send a private message to the specified name.',
			'Syntax: msg "<character name>" <message>',
		},
		
		['noreply'] = {
			'Clear your auto-reply destination.',
			'Syntax: noreply',
		},

		['score'] = {
			'Display the current count of conquered sectors in Deneb.',
			'Syntax: score',
		},
		
		['toch'] = {
			'Send a message to a channel other than the default channel.',
			'Syntax: toch <channel number> <message>',
		},
		
		['unignore'] = {
			'Remove a name from your ignore list, or display current ignore list.',
			'Syntax: unignore [["]<character name>["]]',
		},

		['unlink'] = {
			'Delete a character link.',
			'Syntax: unlink <character name>',
		},
		
		['_default'] = {
			'Command list: as, ch, help, ignore, join, leave, link, links, list, msg, noreply, score, toch, unlink',
			'"help <command>" will display more information about the specified command.',
		},
		
		['_nohelp'] = {
			'No help is available for that topic.'
		},
	},
	
	['VO'] = {
		['ban'] = {
			'Ban the specified user from using the relay for the given number of seconds.',
			'Syntax: ban <nickname> <seconds> <reason>',
		},
		
		['ignore'] = {
			'Ignore private messages received from the specified name, or display current ignore list.',
			'Syntax: ignore [["]<character name>["]]',
		},

		['link'] = {
			'Link your character with an IRC nickname.',
			'Syntax: link ["]<character name>["]',
		},

		['links'] = {
			'Display the list of links associated with your character.',
			'Syntax: links',
		},

		['msg'] = {
			'Send a private message to the specified name.',
			'Syntax: msg "<character name>" <message>',
		},

		['noreply'] = {
			'Clear your auto-reply destination.',
			'Syntax: noreply',
		},

		['unignore'] = {
			'Remove a name from your ignore list, or display current ignore list.',
			'Syntax: unignore [["]<character name>["]]',
		},

		['unlink'] = {
			'Delete a character link.',
			'Syntax: unlink',
		},
		
		['_default'] = {
			'Command list: ignore, link, links, msg, noreply, unignore, unlink',
		},
		
		['_nohelp'] = {
			'No help is available for that topic.'
		},
	},
}

