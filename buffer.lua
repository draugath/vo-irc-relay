local ti, tr = table.insert, table.remove
local sendchat_delay = 360

irc.VOMsgQueue = {} --irc.VOMsgQueue or {}
function irc.VOMsgQueue:Create(channel)
	irc:DebugPrint('irc.VOMsgQueue:Create()', 'II', 2, true)
	if self[channel] then return end
	
	local chan = {Timer=Timer(), Sending = false, Queue={}, Attempted={}}

	local debug_msg_send = 'irc.VOMsgQueue['..channel..']:Send()'
	local debug_msg_pq = 'irc.VOMsgQueue['..channel..'] process_queue()'

	local function process_queue()
		irc:DebugPrint(debug_msg_pq, 'II', 2, true)

		local m
		if (#chan.Queue > 0) then
			-- Send message to VO
			m = tr(chan.Queue, 1)

			-- COMPLETE THE VO-SIDE NOT ONLINE DETECTION FOR PMs
			if (channel == 'PRIVATE') then 
				local lastattempt = next(chan.Attempted)
				if (lastattempt) then chan.Attempted[lastattempt] = nil end
				chan.Attempted[m.dest:lower()] = {Nickname = m.origin, Sock = m.sock}
			end

			SendChat(m.voprefix..m.msg, channel, m.dest)

			-- Echo message to IRC if necessary
			local except = (not m.echo and m.sock) or nil
			if (m.ircprefix) then irc:IRCBroadcast(m.ircprefix..m.msg, nil, except) end

			-- Wait then check for next message
			chan.Timer:SetTimeout(sendchat_delay, process_queue)
		else
			chan.Sending = false
		end
	end
	
	function chan:Send(m)
		irc:DebugPrint(debug_msg_send, 'II', 2, true)

		ti(self.Queue, m)
		if (not self.Sending) then 
			self.Sending = true
			chan.Timer:SetTimeout(sendchat_delay, process_queue)
			--process_queue()
		end
	end

	self[channel] = chan
end

function irc.VOMsgQueue:Enqueue(m)
	irc:DebugPrint('irc.VOMsgQueue:Enqueue()', 'II', 2, true)

	local msg = strip_whitespace(m.msg or '')
	local dest = m.dest
	local origin = m.origin
	local lorigin = origin and origin:lower()
	local emote = m.emote
	local server = m.sock and m.sock.Address

	local name, faction
	
	if (server) then
		local ndata = irc.IRCNameData.Nicknames[server]
		local cdata = irc.VONameData.Characters
		if (m.identity) then 
			name = m.identity
			faction = cdata[name:lower()].Faction
		elseif (ndata[lorigin] and ndata[lorigin].CurLink) then
			name = ndata[lorigin].CurLink
			faction = cdata[name:lower()].Faction
		else
			name = origin
		end
	end
	
	-- Set the prefix to the prefix so the message is delivered properly to VO.
	local begin = '/me '
	if (dest[1] == 'SYSTEM' or dest[1] == 'GROUP' or dest[1] == 'PRIVATE') then begin = '' end -- they don't support emotes

	-- irc:FormatName() will return '' or nil if warranted.
	local voprefix = begin..irc:FormatName(name, nil, emote)
	local ircprefix = irc:FormatName(name, dest[2], emote, faction, true, true)

	local plen = voprefix:len() --+ 4 --len('/me ')
	local slen = 256-plen
	local num = 1

	-- Break the message apart into VO sized chunks.
	for i = 1, 50 do -- don't want to spam chat TOO much -- TODO replace with a while loop?
		local submsg = msg:sub(num, num + slen)
		if (submsg == '') then break end

		local find_start, find_end = submsg:find('%S*$')
		local adjust = find_end - find_start
		if (adjust > 0 and find_end == slen + 1) then 
			submsg = submsg:sub(1, find_start - 1)
		else
			adjust = 0
		end
		
		local banned, reason
		if (server and origin) then banned, reason = irc.IRCNameData:IsBanned(server, lorigin) end

		if (banned) then 
			m.sock:IRCNotice(origin, 'Muted: '..reason..' -- '..banned..' seconds left')
			break
		else
			self[dest[1]]:Send( {
				['msg'] = submsg, 
				['voprefix'] = voprefix,
				['ircprefix'] = ircprefix, -- nil if no message should be sent to IRC
				['origin'] = origin,
				['dest'] = dest[2], 
				['echo'] = m.echo,
				['sock'] = m.sock,         -- the server to be skipped if echo == false.
			} )
			if (server and origin) then irc.IRCNameData:IncrementFloodCount(server, lorigin) end
			num = num + slen - adjust
		end
	end
end


irc.VOMsgQueue:Create("CHANNEL", true)
--irc.VOMsgQueue:Create("SECTOR")
--irc.VOMsgQueue:Create("GROUP")
--irc.VOMsgQueue:Create("GUILD")
irc.VOMsgQueue:Create("PRIVATE")
--irc.VOMsgQueue:Create("SYSTEM")
--irc.VOMsgQueue:Create("STATION")
