local tc, ti, tr = table.concat, table.insert, table.remove

local function ParseLine(line) -- function by jexkerome
	local ln
	local prefix,command
	prefix, ln = line:match("^%:([^ ]+) +(.+)$")
	if prefix then line = ln end
	command, ln = line:match("^([A-Za-z_][^ ]*)(.*)$")
	if not command then
		command, ln = line:match("^([0-9][0-9][0-9])( +.+)$")
		if not command then return else command = tonumber(command) end
	elseif command:gsub("[^-A-Za-z0-9_]", "") ~= command then
		return
	end
	line = ln
	local params = {}
	repeat
		local p
		p, ln = line:match("^ +([^:\10\13 ][^\10\13 ]*)(.*)$")
		if not p then
			p = line:match("^ +:([^\10\13]*)$")
			if not p then break end
			params[#params+1] = p
			break
		else
			params[#params+1] = p
			line = ln
		end
	until false
	return prefix, command, params
end

local function ConnectionMade(conn, error)
	irc:DebugPrint('ConnectionMade()', 'II', 4, true)
	
	--irc:DebugPrint(tc{'connection made -- ',test.stringify(conn)}, '**')
	irc:DebugPrint('connection made -- '..tostring(conn), '**')
	conn:IRCSend("NICK "..irc.Nickname)
	conn:IRCSend("USER "..irc.Username.." 8 * :"..irc.Nickname)
end

local function ConnectionLost(conn)
	irc:DebugPrint('ConnectionLost()', 'II', 4, true)
	
	irc:DebugPrint('connection lost -- '..tostring(conn), '**')
	local address = conn.Address
	local reconnect = irc.Reconnect
	local server = reconnect.Timers[address] or {['Attempt'] = 0, ['Timer'] = Timer()}

	server.Timer:Kill()
	server.Attempt = server.Attempt + 1

	if (reconnect.Attempts < 0 or server.Attempt <= reconnect.Attempts) then
		server.Timer:SetTimeout(reconnect.Time * 1000, function() 
			irc.connect(address)
		end)
		irc:DebugPrint('Reconnecting to '..address..' in '..reconnect.Time..' seconds. ('..server.Attempt..')', 'II')
	else
		server = nil
	end

	reconnect.Timers[address] = server
	irc.disconnect(address, server) -- second argument is "is_reconnecting".  "server" will be nil if not.
end

local function LineReceived(conn, line) -- function for parsing incoming IRC lines
	irc:DebugPrint('LineReceived()', 'II', 4, true)
	
	line = line:gsub('\r', '')
	line = line:gsub('[^%w%p%s]', '')
	irc:DebugPrint(line)
	local prefix, cmd, params = ParseLine(line)
	if (irc.Responses[cmd]) then irc.Responses[cmd](conn, prefix, params) end
end

local sock_methods = {
	['IRCSend'] = function(self, line)
		irc:DebugPrint('sock:IRCSend()', 'II', 2, true)
		irc:DebugPrint(line, '<<')
		self:Send(line..'\r\n')
	end,

	['IRCChat'] = function(self, dest, msg)
		irc:DebugPrint('sock:IRCChat()', 'II', 2, true)
		self:IRCSend('PRIVMSG '..dest..' :'..msg)
	end,

	['IRCNotice'] = function(self, dest, msg)
		irc:DebugPrint('sock:IRCNotice()', 'II', 2, true)
		self:IRCSend('NOTICE '..dest..' :'..msg)
	end,
}

local function AddMethods(sock)
	for method, func in pairs(sock_methods) do sock[method] = func end
end

local function AddOtherData(sock)
	sock.Auth = sock.Auth or {
		['ChanOp'] = {},
		['STATUS'] = {},
		['Reason'] = {},
		['Pending'] = {[0] = {'STATUS'}}, -- store up to 16 names for STATUS call.  send at 16 names or 366 event
		['PendingCmd'] = {},
		['Timer'] = Timer(),
	}
	sock.Channels = sock.Channels or {}
	
	irc.IRCNameData.Nicknames[sock.Address] = irc.IRCNameData.Nicknames[sock.Address] or {}
end

function irc.connect(server, port, channel)
	local servers = irc.Servers
	server = server and tostring(server)
	port = tonumber(port)
	channel = channel and tostring(channel)

	if (server) then 
		servers = { 
			{
				['Address'] = server, 
				['Port'] = port, 
				['Channel'] = channel, 
				['Enabled'] = true
			}, 
			['DefaultPort'] = servers.DefaultPort,
			['DefaultChannel'] = servers.DefaultChannel,
		} 
	end

	for _, s in ipairs(servers) do
		if (s.Enabled) then
			local address = s.Address
			local port = s.Port or servers.DefaultPort
			local sock = TCP.make_client(address, port, ConnectionMade, LineReceived, ConnectionLost)
			sock.Address = address
			sock.Channel = s.channel or servers.DefaultChannel
			sock.Connected = false
			AddMethods(sock)
			AddOtherData(sock)

			irc.Sockets[address] = sock
		end
	end
	irc:RegisterChatEvents()
	if (irc.RequireAuth) then irc.SaveDataTimer:SetTimeout(irc.SaveDataDelay * 1000, irc.SaveData) end

end

function irc.disconnect(server, is_reconnecting)
	local function disconn(server, sock)
		--sock:IRCSend('QUIT :Going down')
		sock.tcp:Disconnect()
		irc.Sockets[server] = nil
	end
	
	local sock = irc.Sockets[server]
	if (sock) then
		disconn(server, sock)
	else
		for server, sock in pairs(irc.Sockets) do disconn(server, sock) end
		for _, server in pairs(irc.Reconnect.Timers) do server.Timer:Kill() end
		irc.Reconnect.Timers = {}
	end

	if (not next(irc.Sockets)) then 
		irc:UnregisterChatEvents()
		irc.SaveDataTimer:Kill()
		irc.SaveData()
	end
end

-- update the methods if already connected
for _, sock in pairs(irc.Sockets) do
	AddMethods(sock)
	AddOtherData(sock)
end