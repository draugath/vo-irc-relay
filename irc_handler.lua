local tc, ti, ts = table.concat, table.insert, table.sort

local function ParsePrefix(prefix)
	return prefix:match('^(.*)!(.+)@(.+)') -- nickname!username@host
end

local function ParseMessage(msg)
	local cmd, nmsg = msg:match('^(%S+) (.*)')
	cmd = cmd or msg
	return cmd:lower(), nmsg
end

local function ExecuteAfterConnectActions(sock)
	sock:IRCSend('MODE '..irc.Nickname..' +Bdip'..(irc.RequireAuth and 'R' or '')) -- See Unreal IRCd documentation about User Modes
	sock:IRCSend('JOIN '..sock.Channel)
	sock.Connected = true
end


irc.Responses = {

	[2] = function(sock, _, params) -- RPL_YOURHOST (RFC2812)
			irc:DebugPrint('irc.Responses["002"]()', 'II', 3, true)

			local version = params[#params]:match('running version (%a+)'):lower()
			sock.Version = version
		end,

	[307] = function(sock, _, params) -- RPL_WHOISREGNICK (Unreal, Bahamut)
			irc:DebugPrint('irc.Responses["307"]()', 'II', 3, true)
			-- params = {dest, nick, "is a registered nick"}
			-- only received from WHOIS if the nick is registered

			if (sock.Version == 'unreal') then --Version is set in 002
			end
		end,
	
	[311] = function(sock, _, params) --RPL_WHOISUSER (RFC1459)
			irc:DebugPrint('irc.Responses["311"]()', 'II', 3, true)
			--params = {dest, nick, user, host, "*", realname}
			-- this numeric reply will be followed by 307 if the nick is registered
		end,

	[318] = function (sock, _, params) --RPL_ENDOFWHOIS (RFC1459)
			irc:DebugPrint('irc.Responses["318"]()', 'II', 3, true)
			-- params = {dest, nicklist, "End of /WHOIS list."} -- nicklist is the same comma-separated list initially supplied to WHOIS
		end,

	[353] = function(sock, _, params) --RPL_NAMREPLY (RFC1459 / RFC2812)
			irc:DebugPrint('irc.Responses["353"]()', 'II', 3, true)
			-- params = {dest, ("@"|"*"|"="), channel, nicklist} -- nicklist is a space-separated list of nicknames
			
			local nicklist = params[4]
			for nick in nicklist:gmatch('%S+') do
				local chanop = nick:match('^@') and true
				nick = nick:gsub('[%+%%@]', '')
				sock.Auth.ChanOp[nick] = chanop
				
				irc:QueueNameForAuth(sock, nick, 353)
			end
		end,

	[366] = function(sock, _, params) -- RPL_ENDOFNAMES (RFC1459)
			irc:DebugPrint('irc.Responses["366"]()', 'II', 3, true)
			-- params = {dest, channel, "End of /NAMES list."}
			if (#sock.Auth.Pending[0] > 1) then irc:Authenticate(sock, 0) end
		end,

	[376] = function(sock, _, params) --RPL_ENDOFMOTD (RFC1459)
			irc:DebugPrint('irc.Responses["376"]()', 'II', 3, true)

			irc.Reconnect.Timers[sock.Address] = nil
			if (irc.DesiredNickname) then 
				sock:IRCChat('NickServ', 'GHOST '..irc.DesiredNickname..' '..irc.Password)
			else
				ExecuteAfterConnectActions(sock)
			end
		end,
				

	[401] = function(sock, _, params) -- ERR_NOSUCHNICK (RFC1459)
			irc:DebugPrint('irc.Responses["401"]()', 'II', 3, true)
			-- params = {dest, nickname, "No such nick/channel"}
			if (params[2] == 'NickServ') then
			end
		end,
				
	[433] = function(sock) -- ERR_NICKNAMEINUSE (RFC1459)
			irc:DebugPrint('irc.Responses["433"]()', 'II', 3, true)
			-- params = {dest, nickname, "Nickname is already in use."}

			if (irc.Password) then irc.DesiredNickname = irc.Nickname end
			irc.Nickname = irc.Nickname..'_'
			sock:IRCSend('NICK '..irc.Nickname)
		end,

	['JOIN'] = function(sock, prefix, params)
			irc:DebugPrint('irc.Responses.JOIN()', 'II', 2, true)
			-- params = {channel}

			local origin = ParsePrefix(prefix)
			if (origin ~= irc.Nickname) then
				-- Query Nickserv for registered nick
				-- I should probably store the host from the prefix to facilitate quick authentication if multiple NICK messages are received
				-- For now I will not keep any records and always reauthenticate with STATUS
				irc:QueueNameForAuth(sock, origin, 'JOIN')

				--sock.Auth.Reason[origin] = 'JOIN'
				--sock:IRCChat('nickserv', 'STATUS '..origin)
			end
		end,
				
	['MODE'] = function(sock, prefix, params)
			irc:DebugPrint('irc.Responses.MODE()', 'II', 2, true)
			-- params = {channel, (-|+)mode(s), nick(s)}
		-->> :ChanServ!services@services.slashnet.org MODE #vorelay +oa draugath draugath%0A
			local origin = ParsePrefix(prefix)
			
			if (origin == 'ChanServ') then
				local mode = params[2]
				local nick = params[3]
				if (mode and mode:match('^%+o')) then
					sock.Auth.ChanOp[nick] = true
				elseif (mod and mode:match('^%-o')) then
					sock.Auth.ChanOp[nick] = nil
				end
			end
		end,

	['NICK'] = function(sock, prefix, params)
			irc:DebugPrint('irc.Responses.NICK()', 'II', 2, true)
			-- params = {new_nick}

			local origin = ParsePrefix(prefix)
			local new_nick = params[1]
			local ndata = irc.NameData
			-- Keep this block, it may be important later even though the underlying functions and tables are changed/gone
-- 			if (origin ~= irc.Nickname) then
-- 				ndata:Initialize('IRC', new_nick)
-- 				local old_ban = ndata.IRC[origin].Ban
-- 				if (old_ban) then
-- 					ndata.IRC[new_nick].Ban = old_ban
-- 				end
-- 			end

			-- I should probably store the host from the prefix to facilitate quick authentication if multiple NICK messages are received
			-- For now I will not keep any records and always reauthenticate with STATUS
			if (sock.Auth.STATUS[origin]) then 
				sock.Auth.ChanOp[origin] = nil
				sock.Auth.STATUS[origin] = nil
				sock.Auth.Reason[origin] = nil
			end
			irc:QueueNameForAuth(sock, new_nick, 'NICK')
			
			--sock.Auth.Reason[new_nick] = 'NICK'
			--sock:IRCChat('nickserv', 'STATUS '..new_nick)
		end,

	['NOTICE'] = function(sock, prefix, params)
			irc:DebugPrint('irc.Responses.NOTICE()', 'II', 2, true)
			-- params = {dest, msg}
			-- msg = "This nickname is registered and protected.  If it is your nickname, type /msg NickServ IDENTIFY password.  Otherwise, please choose a different nickname."
			-- msg = "Password accepted -- you are now recognized."
			-- msg = "STATUS <nickname> <0-3>"
			
			local origin, user, host = ParsePrefix(prefix)
			local dest, msg = params[1], params[2]
			
			if (origin == 'NickServ' and dest == irc.Nickname) then
				if (msg:match('^STATUS')) then
					local nick, status = msg:match('STATUS (%S+) (%d)')
					local lnick = nick:lower()
					status = tonumber(status)
					if (sock.Auth.Reason[nick]) then

						local ndata = irc.IRCNameData.Nicknames[sock.Address]
						if (status > 1 and irc.RequireAuth) then
							if (not ndata[lnick]) then 
								irc.IRCNameData:Initialize(sock.Address, nick) 
							end
							if (next(ndata[lnick].LinkedTo)) then
								status = 4
							end
						elseif (not irc.RequireAuth) then
							if (not ndata[lnick]) then irc.IRCNameData:Initialize(sock.Address, nick) end
							-- TODO disable SaveData
						end

						sock.Auth.STATUS[nick] = status

						if (sock.Auth.Reason[nick] == 'REAUTH') then
							
							local cmd, data
							for _, action in ipairs(sock.Auth.PendingCmd[nick]) do 
								cmd = action[1]
								data = action[2]
							end

							local chanop = sock.Auth.ChanOp[nick]
							
							local cl = irc.CommandLevels[cmd]
							if (not cl or status >= cl or (cl == 100 and chanop)) then
								irc.Commands[cmd](sock, data)
							else
								if (status < 2) then
									sock:IRCNotice(nick, 'You must identify to a registered nick to use this command.')
								elseif (status < 4) then
									sock:IRCNotice(nick, 'You must link to a Vendetta Online character to use this command.')
								elseif (status == 4 and irc.IRCNameData.Nicknames[sock.Address][lnick].CurLink) then
									sock:IRCNotice(nick, 'You must select a new identity with the "as" command.')
								elseif (cl == 100 and not chanop) then
									sock:IRCNotice(origin, 'You must be a channel-op to use this command.')
								else 
									console_print('\n\n!!!!!!!!!!!!!!!!!!!!!!\nsomething went wrong during REAUTH\n!!!!!!!!!!!!!!!!!!!!!\n\n')
								end
							end
							sock.Auth.PendingCmd[nick] = nil
						end
						sock.Auth.Reason[nick] = nil
					end
					
				elseif (msg:match('^This nickname is registered and protected.') and irc.Password ~= '') then -- identify
					sock:IRCChat(origin, 'IDENTIFY '..irc.Password)

				elseif (msg == 'Password accepted -- you are now recognized.') then
					ExecuteAfterConnectActions(sock)
	
 				elseif (msg:match("^Ghost with your nickname has been killed.") and irc.DesiredNickname) then -- successfully ghosted
					sock:IRCSend('NICK '..irc.DesiredNickname)
					irc.Nickname = irc.DesiredNickname
					irc.DesiredNickname = nil
				else
				end
			end

		end,

	['PART'] = function(sock, prefix, params)
			irc:DebugPrint('irc.Responses.PART()', 'II', 3, true)
			-- params = {channel, msg}

			local origin = ParsePrefix(prefix)
			local lorigin = origin:lower()
			if (origin ~= irc.Nickname) then
				sock.Auth.ChanOp[origin] = nil
				sock.Auth.Reason[origin] = nil
				sock.Auth.STATUS[origin] = nil
				sock.Auth.PendingCmd[origin] = nil
			else
				-- if relay PARTs the channel, clear the status list.
				-- TODO add support for multiple channels
				sock.Auth.Reason = {}
				sock.Auth.STATUS = {}
				sock.Auth.Pending = {[0] = {'STATUS'}}
				sock.Auth.PendingCmd = {}
			end
		end,

	['PING'] = function(sock, _, params)
			irc:DebugPrint('irc.Responses.PING()', 'II', 3, true)

			sock:IRCSend('PONG :'..params[1])
		end,

	['PRIVMSG'] = function(sock, prefix, params)
			irc:DebugPrint('irc.Responses.PRIVMSG()', 'II', 3, true)
			-- params = {dest, msg}
			local origin, _, host = ParsePrefix(prefix)
			local lorigin = origin:lower()

			if (irc.Ignore.Host[host] or irc.Ignore.Nick[origin]) then return end

			local dest, msg = params[1], params[2]
			local cmd, nmsg, private

			if (not sock.Auth.STATUS[origin]) then return sock:IRCNotice(origin, 'Nickname authentication in progress.  Please try again in a few seconds.') end
			
			if (dest == irc.Nickname) then
				-- Processing request through private message
				cmd, nmsg = ParseMessage(msg)

				if (not irc.Commands[cmd]) then
					local ndata = irc.IRCNameData.Nicknames[sock.Address][lorigin]

					if (ndata and ndata.AutoReply) then
						if (ndata.AutoReply.As) then
							cmd = 'as'
							nmsg = '"'..ndata.AutoReply.As..'" msg "'..ndata.AutoReply.Dest..'" '..msg

						else
							cmd = 'msg'
							nmsg = msg
						end

					else
						cmd = 'ch'
						nmsg = msg
					end
				end
				private = true
			else
				-- Processing request through public trigger
				local count
				msg, count = msg:gsub('^!', '')
				if (count == 1) then
					private = false
					cmd = 'ch'
					nmsg = msg
				end
			end
			
			local data = {
				['source'] = 'IRC',
				['origin'] = {
					['nickname'] = origin,
					['channel'] = dest,
				},
				['msg'] = nmsg,
				['private'] = private,
			}

			local status = sock.Auth.STATUS[origin]
			local chanop = sock.Auth.ChanOp[origin]

			local cl = irc.CommandLevels[cmd]
			if (not cl or status >= cl or (cl == 100 and chanop)) then
				console_print(cmd) -- I've been seeing errors pop up, lets get the command that's causing it.
				irc.Commands[cmd](sock, data)
			else
				if (status < 2) then
					irc:QueueNameForAuth(sock, origin, 'REAUTH', {cmd, data})
				elseif (status < 4) then
					sock:IRCNotice(origin, 'You must link to a Vendetta Online character to use this command.')
				elseif (status == 4 and not irc.IRCNameData.Nicknames[sock.Address][lorigin].CurLink) then
					sock:IRCNotice(origin, 'You must select a new identity with the "as" command.')
				elseif (cl == 100 and not chanop) then
					sock:IRCNotice(origin, 'You must be a channel-op to use this command.')
				else 
					console_print('\n\n!!!!!!!!!!!!!!!!!!!!!!\nsomething went wrong\n!!!!!!!!!!!!!!!!!!!!!\n\n')
				end
			end
		end,
				
	['QUIT'] = function (sock, prefix, params)
			irc:DebugPrint('irc.Responses.QUIT()', 'II', 3, true)
			-- params = {msg}
			local origin = ParsePrefix(prefix)

			sock.Auth.ChanOp[origin] = nil
			sock.Auth.Reason[origin] = nil
			sock.Auth.STATUS[origin] = nil
			sock.Auth.PendingCmd[origin] = nil
				
		end,
}
