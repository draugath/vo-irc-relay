local rInt, rStr = gkini.ReadInt, gkini.ReadString

local function decode_string(str)
	str = str:gsub('%%e', '')
	return str:gsub('%%s', ' ')
end

local branch = 'irc_relay'

irc = irc or {iniBranch = branch}
irc.SystemNotesID = -1000000
irc.PluginDirectory = rStr(branch, 'PluginDirectory', 'plugins/newirc/') -- make sure to include the trailing slash
irc.Servers = {
	['DefaultChannel'] = rStr(branch, 'Servers.DefaultChannel', '#relay'), --!!!! Change this
	['DefaultPort'] = rInt(branch, 'Servers.DefaultPort', 6667),
	{
		['Address'] = 'irc.slashnet.org',
		['Enabled'] = true,
		['Port'] = 6667,
		--['channel'] = '#vrelay', -- don't use this.  set the DefaultChannel instead
	},
}

irc.Username = rStr(branch, 'Username', 'relay') --TODO give this a better default later
irc.Nickname = rStr(branch, 'Nickname', 'relay')
irc.Password = rStr(branch, 'Password', '')

irc.AutoStart = rInt(branch, 'AutoStart', 0) == 1
irc.Trigger = rStr(branch, 'Trigger', '!') -- Deprecated, but kept just on the off chance.
irc.NonRelayTrigger = rStr(branch, 'NonRelayTrigger', '#')
irc.EchoPublic = rInt(branch, 'EchoPublic', 0) == 1 -- 0 or 1
irc.Debug = rInt(branch, 'Debug', 0) -- not a boolean value
irc.HidePMs = rInt(branch, 'HidePMs', 1) == 1
irc.LocalOnly = rInt(branch, 'LocalOnly', 0) == 1
irc.VOMsgPrefix = decode_string(rStr(branch, 'VOMsgPrefix', '[IRC]%s')) -- '[IRC]'

irc.RequireAuth = rInt(branch, 'RequireAuth', 1) == 1
irc.RequireLink = rInt(branch, 'RequireLink', 1) == 1
irc.AuthDelay = rInt(branch, 'AuthDelay', 1) -- seconds
irc.ReauthDelay = rInt(branch, 'ReauthDelay', 1) -- seconds

irc.ExpireLink = rInt(branch, 'ExpireLink', 30)-- * ExpireLinkMultiplier
irc.ExpireLinkMultiplier = rInt(branch, 'ExpireLinkIncrement', 86400) -- seconds == 1day

irc.VOConnected = false -- TODO Switch back to IsConnected()?

irc.CommandName = rStr(branch, 'CommandName', 'irc')

irc.FloodControl = irc.FloodControl or {
	Enabled = rInt(branch, 'Flood.Enabled', 1) == 1, -- 0 or 1
	Count = rInt(branch, 'Flood.Count', 10), -- number of messages in a given period to be considered flooding
	Period = rInt(branch, 'Flood.Period', 30), -- seconds
	BanLength = rInt(branch, 'Flood.BanLength', 60), -- seconds
}

irc.Cleanup = irc.Cleanup or {}

irc.VOPMQueue = irc.VOPMQueue or {}

irc.VOChannels = irc.VOChannels or { 
	['Default'] = rInt(branch, 'Channels.Default', 100), --TODO change this to 100 later
	['Forbidden'] = { [11] = true, [50434300] = true }, --list of channels that should never be relayed if joined
	['Map'] = {--[[
		<string> = {<irc_channel>,},
	]]},
	['Joins'] = {--[[
		<irc_channel> = <string>,
	]]},
	
	-- Moved the below to vo_handler.lua
	-- ['Details'] = {},
	-- ['List'] = {},
	-- ['Max'] = rInt(branch, 'VOChannels.Max', 32)
}

irc.Sockets = irc.Sockets or {}

irc.VONameData = irc.VONameData or {
	['Characters'] = {--[[
	[<charactername>] = { -- lowercase
		['ProperName'] = <CharacterName>, -- Proper case
		['Faction'] = <number>,
		['Ignore'] = {
			[<CharacterName>] = true, -- lowercase
		},
		['PendingLinks'] = {},
		['LinkedTo'] = {
			[<server_address>] = <string>, -- IRC Nickname lowercase
		},
		['AutoReply'] = <CharacterName>, -- natural case
	]]},
}

irc.IRCNameData = irc.IRCNameData or {
	['Nicknames'] = {--[[
		[<server_address>] = {
			[<nickname>] = { -- lowercase
				['ProperName'] = <Nickname>, -- natural case
				['Ignore'] = {
					[<CharacterName>] = true, -- natural case
				},
				['CurLink'] = <CharacterName>, -- natural case
				['LinkedTo'] = {
					[<charactername>] = true, -- lowercase
				},
				['Ban'] = {
					['Time'] = <number>, -- timestamp when they can talk again
					['Reason'] = <string>,
				},
				['FloodQueue'] = {},
				['CurChannel'] = <number>,
				['Channels'] = {
					[<number>] = true,
				},
				['AutoReply'] = {
					['Dest'] = <CharacterName>, -- natural case
					['As'] = <CharacterName>, -- natural case
				},
			},
		},
	]]},
}
	
irc.SaveDataTimer = irc.SaveDataTimer or Timer()
irc.SaveDataDelay = rInt(branch, 'SaveDataDelay', 300) -- seconds

irc.VODevelopers = irc.VODevelopers or {
	['Incarnate'] = true,
	['raybondo'] = true,
	['momerath42'] = true,
}

irc.MuteList = irc.MuteList or {}

irc.Ignore = {
	Nick = {
		-- ['tristen1229'] = true, 
	},
	Host = {
		-- ["cloak-B627BAFA.direcpc.com"] = true
	},
}
	
irc.KillSwitchACL = {
}

irc.Reconnect = irc.Reconnect or {['Timers'] = {}}
irc.Reconnect.Time = rInt(branch, 'Server.Reconnect.Time', 20) -- seconds
irc.Reconnect.Attempts = rInt(branch, 'Server.Reconnect.Attempts', 10)
