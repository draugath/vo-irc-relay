local PLUGIN_DIR = ''

local function load_file(file)
	assert(type(file) == 'string')
	console_print('[IRC] Loading '..file)
	dofile(PLUGIN_DIR..file)
end

-- set file load order here
local files = {
	'vars.lua',
	'functions.lua',
	'tcpsock.lua',
	'buffer.lua',
	'relay_cmds.lua',
	'custom_cmds.lua',
	'irc_handler.lua',
	'vo_handler.lua',
	'clientctl.lua',
	'cli.lua',
	'help.lua',
}

local files_by_name = {}
for _, v in ipairs(files) do files_by_name[v:match('^([%w_]+)%.lua')] = v end

local function PostLoadSetup()
	PLUGIN_DIR = irc.PluginDirectory
	UnregisterEvent(PostLoadSetup, 'PLUGINS_LOADED')
end

local function PreReloadInterfaceCleanup()
	if (next(irc.Sockets)) then irc.disconnect() end
end

RegisterEvent(PostLoadSetup, 'PLUGINS_LOADED')
RegisterEvent(PreReloadInterfaceCleanup, 'UNLOAD_INTERFACE')

local function irc_reload(_, params)
	local file
	for _, key in ipairs(params) do
		file = files_by_name[key]
		if (file) then 
			pcall(irc.Cleanup[key])
			load_file(file)
		end
	end
end
RegisterUserCommand('irc_reload', irc_reload)

-- load irc relay files
console_print('[IRC] Initializing Relay')
for _, file in ipairs(files) do load_file(file) end
